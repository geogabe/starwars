// ======================= list of all the characters in star wars movies =======================

function HeightChart(data) {
    render(data);

    function render(data) {
        d3.select("#people").selectAll("*").remove();

        var canvas = d3.select("#people").append("svg")
            .attr("id", "characters")
            .attr("width", 1000)
            .attr("height", 300)
            .style("padding", 10);

        var div = d3.select("#character").append("div")
            .attr('class', "tooltip2")
            .style("opacity", 0);


        canvas.selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("width", 2)
            .attr("transform", "translate(0, 0)")
            .attr("xlink:href", function (d) {
                return d.url;
            })
            .style("opacity", 0.3)
            .attr("height", 0)
            .on("click", function (d, i) {
                window.location = d.url;
            })
            .attr("x", function (d, i) {
                return i * 10;
            })
            .attr("fill", function (d) {
                return d.gender == "female" ? "#FF1493" :
                    d.gender == "male" ? "#1E90FF" :
                    d.gender == "hermaphrodite" ? "#ffa906" :
                    "#4dd6ac"
            })
            .on("mouseover", function (d) {
                div.transition()
                    .duration(200)
                    .style('opacity', .9);
                div.html((d.name) + " - " + "Height: " + (d.height) + "cm");
                d3.select(this).transition().duration(300)
                    .attr("transform", "translate(0,10)")
                    .style("opacity", 0.5);
            })
            .on("mouseout", function () {
                div.transition()
                    .duration(500)
                    .style('opacity', 0)
                d3.select(this).transition().duration(300)
                    .attr("transform", "translate(0,0)")
                    .style("opacity", 1);
            })
            .transition()
            .delay(function (d, i) {
                return i * 10
            })
            .duration(200)
            .attr('height', function (d) {

                return d.height;
            })
            .style('opacity', 1);

    }

    return {
        render: render
    }
};

function customersController($scope, $http) {
    //            $http.get("http://www.w3schools.com//website/Customers_JSON.php")
    var chart, currentData, originalData;
    $scope.$watch('order', function (order) {
        if (chart) {
            currentData = _.sortBy(currentData, order.key);
            if (order.reverse) {
                currentData.reverse();
            }
            chart.render(currentData);
        }
    });

    $scope.$watch('filterName', function (filter) {
        if (!chart)
            return;
        if (filter) {
            var re = new RegExp(filter, "i");
            currentData = originalData.filter(function (d) {
                return re.test(d.name) || re.test(d.gender);
            })

            chart.render(currentData);
        } else {
            chart.render(originalData);
        }
    })

    $http.get("../data/people.json")
        .success(function (data) {
            data = _.sortBy(data, $scope.order.key)
            $scope.people = originalData = currentData = data;
            chart = new HeightChart(data);
        });

    $scope.orders = [
        {
            id: 1,
            title: 'name Ascending',
            key: 'name',
            reverse: false
    },
        {
            id: 2,
            title: 'name Descending',
            key: 'name',
            reverse: true
    },
        {
            id: 3,
            title: 'Height Ascending',
            key: 'height',
            reverse: false
    },
        {
            id: 4,
            title: 'Height Descending',
            key: 'height',
            reverse: true
    },
        {
            id: 5,
            title: 'Gender Ascending',
            key: 'gender',
            reverse: false
    },
        {
            id: 6,
            title: 'Mass Ascending',
            key: 'mass',
            reverse: false
    }
];
    $scope.order = $scope.orders[4];
};


// ======================= PLanets and people visualization =======================

d3.json("data/planets.json", function (data) {
    var planetsCanvas = d3.select("#vizPLanets").append("svg")
        .attr("id", "planets")
        .attr("width", 1300)
        .attr("height", 2500)
        .style("padding-left", 50);

    //                            var color = d3.scale.linear()
    //                                .domain([0, 50, 100])
    //                                .range(["#FF3E96", "#4dd6ac", "#3cb3ff"]);


    var div = d3.select("#planet").append("div")
        .attr('class', "tooltip")
        .style("opacity", 0);

    var circles = planetsCanvas.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")

    //Define sort order flag
    var sortOrder = false;

    //Define sort function
    var sortPlanets = function () {

        //Flip value of sortOrder
        sortOrder = !sortOrder;

        circles.sort(function (a, b) {
                if (sortOrder) {
                    return d3.ascending(a, b);
                } else {
                    return d3.descending(a, b);
                }
            })
            .transition()
                .delay(function (d, i) {
                    return i * 20;
                })
                .duration(1000)
                .attr("cx", function (d, i) {
                    return i * 20
                });

    };

    var circleAttributes = circles
        .attr("cx", function (d, i) {
            return i * 20
        })
        .attr("cy", 150)
        .attr("r", 0)
        .style("fill", function (d) {
            if (d.surface_water <= 30) {
                return "#FF1493"
            } else if (d.surface_water <= 60) {
                return "#4dd6ac"
            } else if (d.surface_water >= 61) {
                return "#1E90FF"
            };

        })
        .style("opacity", 0)
        .on("mouseover", function (d) {
            d3.select("rect").transition()
                .attr("width", 6);
            d3.select(this).transition()
                .duration(200)
                .attr("r", function (d) {
                    return (d.diameter / 1000) + 10;
                })
                .style("opacity", 0.9);
            div.transition()
                .duration(200)
                .style("opacity", .9);
            div.html("Planet " + (d.name) + " - " + "Population: " + (d.population));
        })
        .on("mouseout", function (d) {
            d3.select("rect").transition()
                .attr("width", 2);
            div.transition()
                .duration(500)
                .style("opacity", 0);
            circles.transition()
                .attr("r", function (d) {
                    return (d.diameter / 1000);
                })
                .style("opacity", 0.7);
        })
        .on("click", function () {
            sortPlanets();
        })
        .transition()
        .delay(function (d, i) {
            return i * 30
        })
        .duration(500)
        .attr("r", function (d) {
            return (d.diameter / 1000);
        })
        .style("opacity", 0.7);



    circles
        .append("text")
        .attr("x", function (d, i) {
            return i * 20
        })
        .style("text-anchor", "middle")
        .attr("dy", 0.15+"em")
        .style("font-size", 12)
        .text(function (d) {
            return (d.name + " - " + d.climate)
        })
        .style("fill", "white");


    //  =======================   population barchart =======================


    planetsCanvas.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d, i) {
            return i * 20;
        })
        .attr("width", 2)
        .attr('height', 0)
        .style("opacity", 0)
        .attr("transform", "translate(0,150)")
        .style("fill", "#3cb3ff")
        .transition()
        .delay(function (d, i) {
            return i * 30
        })
        .duration(500)
        .attr('height', function (d) {
            return (d.population / 100000);
        })
        .style("opacity", 0.7);
});