// ======================= PLanets and people visualization =======================

d3.json("../data/planets.json", function (data) {
    var planetsCanvas = d3.select("#vizPLanets").append("svg")
        .attr("width", 2000)
        .attr("height", 500)
        .style("padding-left", 50);

    //                            var color = d3.scale.linear()
    //                                .domain([0, 50, 100])
    //                                .range(["#FF3E96", "#4dd6ac", "#3cb3ff"]);

    var circles = planetsCanvas.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")

    var circleAttributes = circles
        .attr("cx", function (d, i) {
            return i * 20
        })
        .attr("cy", 200)
        .attr("r", 0)
        .style("fill", function (d) {
            return d.surface_water <= 30 ? "#FF3E96" :
                31 <= d.surface_water <= 60 ? "#4dd6ac" :
                "#3cb3ff";
        })
        .style("opacity", 0)
        //                .on("mouseover", function (d) {
        //
        //
        //                })
        .transition()
        .delay(function (d, i) {
            return i * 30
        })
        .duration(500)
        .attr("r", function (d) {
            return (d.diameter / 1000);
        })
        .style("opacity", 0.7);


    circles
        .append("text")
        .attr("x", function (d, i) {
            return i * 20
        })
        .style("text-anchor", "middle")
        .attr("dy", function (d) {
            return (d.diameter / 10000000) + 10
        })
        .style("font-size", 12)
        .text(function (d) {
            return (d.name + " - " + d.climate)
        })
        .style("fill", "white");



    //  =======================   population barchart =======================
    
    
    var popChart = d3.select("#VizPop").append("svg")
        .attr("width", 2000)
        .attr("height", 2000)
        .style("padding-left", 50);


    popChart.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", function (d, i) {
            return i * 20;
        })
        .attr("width", 2)
        .attr('height', 0)
        .style("opacity", 0)
        .style("fill", "#3cb3ff")
        .transition()
        .delay(function (d, i) {
            return i * 30
        })
        .duration(500)
        .attr('height', function (d) {
            return (d.population / 10000);
        })
        .style("opacity", 0.7);


});